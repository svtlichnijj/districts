<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	 * Generate token
	 */
	public function generateToken(): void
	{
		$this->attributes['api_token'] = str_random(60);
		$this->save();
	}

	public function getApiToken(): string
	{
		return $this->attributes['api_token'] ?? '';
	}

	public function clearApiToken(): void
	{
		$this->attributes['api_token'] = null;
		$this->save();
	}
}
