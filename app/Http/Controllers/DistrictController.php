<?php

namespace App\Http\Controllers;

use App\District;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
	public function store(Request $request): JsonResponse
	{
		$district = District::create($request->all());

		return response()->json($district, 201);
	}

	public function index(): string
	{
		return app(District::class)->take(100)->get()->toJson();
	}

	public function create()
	{
		//
	}

	public function show(District $district): District
	{
		return $district;
	}

	public function update(Request $request, District $district): JsonResponse
	{
		$district->update($request->all());

		return response()->json($district);
	}

	public function destroy(District $district): JsonResponse
	{
		try {
			$district->delete();
		} catch (Exception $exception) {
			return response()->json([
				'message' => $exception->getMessage(),
				'errors' => $this->validator->messages()->getMessages(),
			], 422);
		}

		return response()->json(null, 204);
	}

	public function edit()
	{
		//
	}
}
