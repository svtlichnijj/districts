<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers {
    	register as protected parentRegister;
	}

	private $validator;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected string $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
	 */
    protected function validator(array $data)
	{
		$this->validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

		return $this->validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
	 *
     * @return User
     */
    protected function create(array $data): User
	{
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

	/**
	 * @param Request $request
	 *
	 * @return JsonResponse|Response
	 */
	protected function register(Request $request)
	{
		try {
			return $this->parentRegister($request);
		} catch (Exception $exception) {
			return response()->json([
				'message' => $exception->getMessage(),
				'errors' => $this->validator->messages()->getMessages(),
			], 422);
		}
	}

	protected function registered(Request $request, User $user): JsonResponse
	{
		$user->generateToken();

		return response()->json(['data' => $user->toArray()], 201);
	}
}
