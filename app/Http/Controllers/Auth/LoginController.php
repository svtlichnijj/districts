<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected string $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

	public function login(Request $request): JsonResponse
	{
		$this->validateLogin($request);

		if ($this->attemptLogin($request)) {
			/** @var User $user */
			$user = $this->guard()->user();
			$user->generateToken();

			return response()->json([
				'data' => $user->toArray(),
			]);
		}

		return $this->sendFailedLoginResponse($request)->toJson();
	}

	public function logout(): JsonResponse
	{
		/** @var User $user */
		$user = Auth::guard('api')->user();

		if ($user) {
			$user->clearApiToken();
		}

		return response()->json(['data' => 'User logged out.']);
	}
}
