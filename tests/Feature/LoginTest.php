<?php

namespace Tests\Feature;

use App\User;
use Faker\Generator;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class LoginTest extends TestCase
{
	protected function sendRequestApiLogin(array $jsonData = []): TestResponse
	{
		return $this->json('post', '/api/login', $jsonData);
	}

	protected function sendRequestApiGetDistricts(string $apiToken): TestResponse
	{
		return $this->withHeader('Authorization', "Bearer $apiToken")
			->json('GET', '/api/districts');
	}

	protected function checkIsSuccessLogin(TestResponse $response): TestResponse
	{
		return $response->assertStatus(200)
			->assertJsonStructure([
				'data' => [
					'id',
					'name',
					'email',
					'created_at',
					'updated_at',
					'api_token',
				],
			]);
	}

	public function testRequiresEmailAndLogin()
	{
		$this->sendRequestApiLogin()
			->assertStatus(422)
			->assertExactJson([
				'message' => 'The given data was invalid.',
				'errors' => [
					'email' => ['The email field is required.'],
					'password' => ['The password field is required.'],
				]
			]);
	}

	public function testUserLoginSuccessfully(): array
	{
		$faker = app(Generator::class);
		$password = Str::random();
		$payload = [
			'email' => $faker->email(),
			'password' => Hash::make($password),
		];
		$user = factory(User::class)->create($payload);
		$payload['password'] = $password;

		$response = $this->sendRequestApiLogin($payload);
		$this->checkIsSuccessLogin($response);

		return [
			'response' => $response,
			'user' => $user,
		];
	}

	public function testUserLoginBearerUnSuccessfully()
	{
		$resultSuccess = $this->testUserLoginSuccessfully();
		/** @var TestResponse $response */
		$response = $resultSuccess['response'];
		$apiToken = $response->decodeResponseJson()['data']['api_token'];

		/** @var User $user */
		$user = $resultSuccess['user'];
		// Simulating logout
		$user->clearApiToken();

		$responseBearer = $this->sendRequestApiGetDistricts($apiToken);
		$responseBearer->assertStatus(401);
		$this->assertEquals(['error' => 'Unauthenticated'], $responseBearer->decodeResponseJson());
	}

	public function testUserLoginBearerSuccessfully()
	{
		$resultSuccess = $this->testUserLoginSuccessfully();
		/** @var TestResponse $response */
		$response = $resultSuccess['response'];
		$apiToken = $response->decodeResponseJson()['data']['api_token'];

		$responseBearer = $this->sendRequestApiGetDistricts($apiToken);
		$responseBearer->assertStatus(200)
			->assertJsonStructure([
				[
					'id',
					'title',
					'body',
					'created_at',
					'updated_at',
				]
			]);
	}
}
