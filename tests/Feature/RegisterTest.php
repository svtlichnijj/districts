<?php

namespace Tests\Feature;

use Faker\Generator;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Str;
use Tests\TestCase;

class RegisterTest extends TestCase
{
	protected function sendRequestApiRegister(array $jsonData = []): TestResponse
	{
		return $this->json('post', '/api/register', $jsonData);
	}

	public function testsRequiresPasswordEmailAndName()
	{
		$this->sendRequestApiRegister()
		     ->assertStatus(422)
		     ->assertJson([
			     'message' => 'The given data was invalid.',
			     'errors' => [
				     'name' => ['The name field is required.'],
				     'email' => ['The email field is required.'],
				     'password' => ['The password field is required.'],
			     ]
		     ]);
	}

	public function testsRequirePasswordConfirmation()
	{
		$faker = app(Generator::class);
		$payload = [
			'name' => $faker->name(),
			'email' => $faker->email(),
			'password' => Str::random(),
		];

		$this->sendRequestApiRegister($payload)
		     ->assertStatus(422)
		     ->assertExactJson([
			     'message' => 'The given data was invalid.',
			     'errors' => [
				     'password' => ['The password confirmation does not match.'],
			     ]
		     ]);
	}

	public function testsRegistersSuccessfully()
	{
		$faker = app(Generator::class);
		$password = Str::random();
		$payloadFragment = $payload = [
			'name' => $faker->name(),
			'email' => $faker->email(),
		];
		$payload += [
			'password' => $password,
			'password_confirmation' => $password,
		];

		$this->sendRequestApiRegister($payload)
			 ->assertStatus(201)
			 ->assertJsonStructure([
				 'data' => [
					 'id',
					 'name',
					 'email',
					 'created_at',
					 'updated_at',
					 'api_token',
				 ],
			 ])
			->assertJsonFragment($payloadFragment);
	}
}
