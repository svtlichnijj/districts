<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class LogoutTest extends TestCase
{
	protected function isUserLogout(User $user)
	{
		$token = $user->getApiToken();
		$this->assertEquals('', $token);
		$headers = [
			'Authorization' => 'Bearer ' . $token,
			'Accept' => 'application/json',
		];

		$responseBearer = $this->withHeaders($headers)
			->json('get', '/api/districts');
		$responseBearer->assertStatus(401);
		$this->assertEquals(['error' => 'Unauthenticated'], $responseBearer->decodeResponseJson());
	}

	public function testUserIsLoggedOutProperly()
	{
		$user = factory(User::class)->create();

		$this->actingAs($user, 'api')
			->json('get', '/api/districts')
			->assertJsonStructure([
				0 => [
					'id',
					'title',
					'body',
					'created_at',
					'updated_at'
				]
			])
			->assertStatus(200);

		$this->json('post', '/api/logout')
			->assertStatus(200);

		$this->refreshApplication();
		$this->isUserLogout($user);
	}

	public function testUserWithNullToken()
	{
		// Simulating login
		$user = factory(User::class)->create();
		/** @var User $user */
		$user->generateToken();
		$this->assertNotNull($user->getApiToken());

		// Simulating logout
		$user->clearApiToken();

		$this->isUserLogout($user);
	}
}
