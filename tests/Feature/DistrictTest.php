<?php

namespace Tests\Feature;

use App\District;
use App\User;
use Carbon\Carbon;
use Faker\Generator;
use Tests\TestCase;

class DistrictTest extends TestCase
{
	public function testsDistrictsAreCreatedCorrectly()
	{
		$dateTimeNow = Carbon::now()->format('Y-m-d H:i:s');
		$faker = app(Generator::class);
		$payload = [
			'title' => $faker->name(),
			'body' => $faker->text(),
			'updated_at' => $dateTimeNow,
			'created_at' => $dateTimeNow,
		];

		$responseDistricts = $this->actingAs(factory(User::class)->create(), 'api')
			->json('POST', '/api/districts', $payload);
		$responseDistricts->assertStatus(201);

		$responseDistrictsArray = $responseDistricts->decodeResponseJson();
		unset($responseDistrictsArray['id']);
		$this->assertEquals($payload, $responseDistrictsArray);
	}

	public function testsDistrictsAreUpdatedCorrectly()
	{
		$dateTimeNow = Carbon::now()->format('Y-m-d H:i:s');
		$faker = app(Generator::class);
		$districtData = [
			'title' => $faker->name(),
			'body' => $faker->text(),
			'updated_at' => $dateTimeNow,
			'created_at' => $dateTimeNow,
		];
		$district = factory(District::class)->create($districtData);

		$payload = [
			'title' => $faker->name(),
			'body' => $faker->text(),
		];

		$responseDistricts = $this->actingAs(factory(User::class)->create(), 'api')
			 ->json('PUT', '/api/districts/' . $district->id, $payload);
		$responseDistricts->assertStatus(200);

		$responseDistrictsArray = $responseDistricts->decodeResponseJson();
		unset($responseDistrictsArray['id']);
		$this->assertEquals(array_merge($districtData, $payload), $responseDistrictsArray);
	}

	public function testsArticlesAreDeletedCorrectly()
	{
		$districtId = factory(District::class)->create()->id;

		$this->actingAs(factory(User::class)->create(), 'api')
			 ->json('DELETE', '/api/districts/' . $districtId)
			 ->assertStatus(204);
		$this->assertDatabaseMissing('districts', ['id' => $districtId]);
	}

	public function testDistrictsAreListedCorrectly()
	{
		$now = Carbon::now()->format('Y-m-d H:i:s');
		$faker = app(Generator::class);
		$districtDataFirst = [
			'title' => $faker->name(),
			'body' => $faker->text(),
			'updated_at' => $now,
			'created_at' => $now,
		];
		factory(District::class)->create($districtDataFirst);

		$nowPlusSecond = Carbon::now()->addSeconds(5)->format('Y-m-d H:i:s');
		$districtDataSecond = [
			'title' => $faker->name(),
			'body' => $faker->text(),
			'updated_at' => $nowPlusSecond,
			'created_at' => $nowPlusSecond,
		];
		factory(District::class)->create($districtDataSecond);

		$responseDistricts = $this->actingAs(factory(User::class)->create(), 'api')
			 ->json('GET', '/api/districts');
		$responseDistricts->assertStatus(200)
			 ->assertJsonStructure([
				 '*' => ['id', 'body', 'title', 'created_at', 'updated_at'],
			 ]);
		$responseDistrictsLast = array_slice($responseDistricts->decodeResponseJson(), -2, 2);
		$responseDistrictsLastWithoutIds = array_map(function (array $responseDistrict) {
				unset($responseDistrict['id']);
				return $responseDistrict;
			},
			$responseDistrictsLast
		);
		$this->assertEquals([$districtDataFirst, $districtDataSecond], $responseDistrictsLastWithoutIds);
	}
}
