<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\District;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	    $this->call('UserTableSeeder');
	    $this->command->info('Таблица пользователей заполнена данными!');

	    $this->call('DistrictTableSeeder');
	    $this->command->info('Таблица округов заполнена данными!');
    }
}

class UserTableSeeder extends Seeder {

	public function run() {
		DB::table( 'users' )->delete();

		$password = Hash::make('test123');

		User::create( [
			'name' => 'foo',
			'email' => 'foo@bar.com',
			'password' => $password
		] );
	}

}

class DistrictTableSeeder extends Seeder {

	public function run() {
		DB::table( 'districts' )->delete();

		District::create( [
			'title' => 'foo',
			'body'  => 'short description',
		] );
	}

}
